Pourquoi je suis intéressé ?
Formation =
acces facile a un travail bien payé

- compétence sur de la création de projet
  pas d'études
  si pas chili je fais pas rien

Pourquoi je suis intéressante ?
Connaissance du monde agile
Déjà créer des projets
outils :Gitlab Github slack
Jeune
Qualités : CNV , Interviews, Social, organiser des atelier collaboratifs, travaille en équipe , bon sens du relationnel

#Base

1. Je suis intéressé
2. Ma vision
3. Donc je vous intéresse
4. Clairement, c'est fait pour moi
   Garder en tête :
   Authenticité, moins d'humilité, vocabulaire de ce monde,

Lettre :
Madame, Monsieur

Au vu de la fiche de poste de Business Developer ou Product Owner,
je suis interessé par le stage que vous proposez.
Je vois en ce stage l'opportunité de solidifier mes compétences en terme de création de projet.

Je suis moins intéressée par le côté business, que par le côté developper.
Déjà sensibilisé par les approches agile et lean, je suis plutôt sur l'idée d'apporter de la valeur au produit que sur l'idée du commercial ou marketing dans lesquelles à la fois mes compétences sont réduite mais l'interêt que j'y porte aussi.
Ma vision : placer l'utilisateur au centre et fabriquer un produit qui répond à un besoin et pour moi, cela ne peut se faire qu'avec l'aide des utilisateurs.

J'en ai déjà fait l'expérience lorsqu'à 16 ans j'ai co-créé Plein Phare un outil d'accompagnement pour les lycéens face aux problématiques d'orientation.
Dans ce projet construit à partir d'interviews de lycéen·ne·s nous avons beaucoup travaillé sur les valeurs et l'intention;à l'aide d'atelier agile et collaboratifs.

Les compétences demandés pour ce stage me parle beaucoup :

-Travail d'équipe : j'ai pour ainsi dire toujours travaillé en équipe "Tout seul on va plus vite ensemble on va plus loin." est devenu un de mes mantras autant pour plein phare que pendant mon service civique d'un an dans une association de lutte contre les discriminations où j'ai pu faire l'expérience d'un binôme très harmonieux lors d'interventions, mais aussi d'une collaboraton avec quelqu'un qui n'avait ni la même culture, ni les mêmes valeurs, ni la même façon de travailler. Cela fut dur mais très instructif j'ai pu ainsi travailler ma patience ainsi que la communication non violente.

-Relationnel : De part un de mes traits de caractère principal l'empathie, je peux dire que j'ai un très bon sens du relationnel, très à l'aise avec les mots et la parole, en langage courant on dit souvent que "Je met les gens à l'aise". J'ai une qualité d'écoute forte qui me permet de bien prendre en compte les besoins exprimés ou sous entendu.

-Communication : Sensibilisé à la communication non violente et à l'écoute empathique, la commuication avec l'autre a peu de secret pour moi. La nécessité d'une posture basse dans le rapport à l'autre est pour moi évident et cela me facilite beaucoup à la fois dans mes projets et ma vie quotidienne.

-Organiser des atelier: Lors de mon service civique je me suis étonnée seule de mon aisance à prendre la parole en public et à gérer un groupe, certe les thèmes étaient plus profond (sexisme, racisme, immigration, handicap), mais nos outils ludiques me font dire qu'une fois qu'on a été animateur la compétence à cette organisation est là.

Autonome et à l'écoute, je fais de mon jeune âge un atout et non un frein. Je suis persuadé que ma place est parmis vous, quand j'entend "Si vous souhaitez faire changer les choses et construire un Etat doté d'outils pour faire face aux défis du XXI ème siècel" ma fougue et mon dynamisme s'emporte et j'entend alors "Louna voilà une possibilité de te développer et de continuer à changer les choses. Et devine quoi ? Tu n'as même pas à t'exclure du système", voilà ce qui me porte et qui me donne envie d'avancer, qui me dit qu'on à besoin de moi quelque part pour faire changer les choses. 





