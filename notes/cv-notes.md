2017-2018 9 mois à Léo Lagrange Médittérannée Pôle engagement. 
Le taf -> Animer des interventions dans les centres de formation (collèges, lycées, etc.) ainsi que l'école de police avec des journées à thème ( Sexisme,Homophobie,Immigration, Préjugé, handicap) développé avec des outils d'éducation populaire, à travers le jeu et le débat.

Les compétences apportées ou validées :
 -> Animation d'un groupe : le gérer, le faire parler et sortir de sa zone de confort.
 -> Adaptabilité : aux différents niveaux, aux différentes idées 
 -> Prise de parole en public : confiance en soi, voix claire, vocabulaire adapté
 -> Travaille d'équipe : en binôme, en trinôme
 -> Créativité : créer des journées, des outils
 -> Organisation : Démarcher, prévoir les différentes interventions, un peu d'administratif
 -> Communication Non Violente : Savoir amener et gérer le débat entre différentes personnes avec des avis différents, savoir répondre au discours violent, haineux


Août 2018 1 mois à Léo Lagrange Vaucluse Pôle Handicap.
Le taf-> Animatrice dans un centre de loisirs composés uniquement d'enfants en situation de TSA (Trouble du Spectre Autistique)

Les compétences apportées ou validées :
-> Sensibilisée au handicap : Comment se comporter, éduquer les autres à bien se comporter, savoir gérer une crise.
-> Patience et adaptabilité : Apprendre à découvrir la personne, comprendre ses besoins (non verbal) et savoir y répondre tout en l'amenant vers la découverte.
-> Compréhension qu'on puisse réfléchir d'une autre manière : découvrir le monde de quelqu'un et y être présent sans s'imposer.
-> Travail d'équipe : 5 ou 6 animateurs très soudés.
-> Rythme de travail soutenu : 8h/17h en non-stop car 1 enfant par animateur
-> Gestion des émotions : Savoir s'oublier si l'enfant fais mal, ou part en crise pour faire passer l'enfant en priorité et ne pas le perturber par les émotions ressentis.
-> Empathie : Prévoir une crise par l'observation de l'enfant et la compréhension de ses angoisses et de ce qui risque de le troubler.