
Qualités
Autonomie
Éloquence
Intégrité
Adaptabilité
Écoute
Énergie
Empathie
 

# Curriculum Vitæ

Je suis Louna, j'ai 19 ans et je viens de Montpellier.

Engagée dans les projets que je porte et les causes qui me tiennent à coeur, j'ai beaucoup  d'enthousiasme à les réaliser. Je prend beaucoup de plaisir dans l'interaction avec l'autre et je porte en moi des valeurs fortes qui font de moi une personne intègre.

## Formation

- 2017 : Bac Littéraire - Mention bien
- 2017-2018 : Fac d'anthropologie - L1

## Expériences professionnelles

- Août 2018 - 1 mois à [Léo Lagrange](http://www.leolagrange.org/bienvenue-au-kaleidoscope/) Vaucluse Pôle Handicap.
 Animatrice dans un centre de loisirs composés uniquement d'enfants en situation de TSA (Trouble du Spectre Autistique)

Les compétences apportées ou validées :
- Sensibilisation au handicap : Comment se comporter, savoir gérer une crise.
- Patience et adaptabilité : Apprendre à découvrir la personne, comprendre ses besoins.
- Travail d'équipe : Organisation avec 5 ou 6 animateurs très soudés.
- Rythme de travail soutenu : 8h/17h en continu, 1 enfant par animateur.
- Gestion des émotions : Savoir gérer ses émotions lorsque l'enfant fais mal, ou part en crise et ne pas le perturber par ses ressentis.
- Empathie : Prévoir une crise par l'observation de l'enfant et la compréhension de ses angoisses et de ce qui risque de le troubler. Découvrir le monde de quelqu'un sans s'imposer.



- 2017-2018 Service Civique de 9 mois à [Léo Lagrange](https://www.democratie-courage.org/) Méditerranée Pôle engagement. 
Mon travail consistait à animer des interventions en milieu scolaire et dans les écoles de police sur les thèmes du sexisme, de l'homophobie, de l'immigration et du handicap avec des outils d'éducation populaire, à travers le jeu et le débat.

Les compétences apportées ou validées :
- Animation de groupe : Permettre à tout le monde de s'exprimer et d'échanger sur des sujets polémiques, prendre la parole en public.
- Travail d'équipe : S'organiser et intervenir en binome et en groupe.
- Autonomie et organisation : Créer des journées des outils, démarcher, prévoir les différentes interventions, rédiger les bilans

- 2016-2018 Co-créatrice de [Pleinphare](https://www.pleinphare.xyz/)
Outil original d'aide à l'orientation des lycéens.
-Faire d'une idée un projet
-créer en groupe : s'accorder sur les intentions, les valeurs 




- Depuis toujours au contact du monde agile Papa : http://scopyleft.fr/
- Depuis toujours au contact de la musique Maman : http://alcyon.barbara.free.fr/index.html
- Depuis toujours au contact des enfants : proche de la famille beaucoup de baby siting, + animation


Quels sont mes compétences ?
-> Animation de groupe
-> Adaptabilité 
-> Prise de parole en public 
-> Créativité
-> Organisation
-> Communication Non Violente
-> Sensibilisée au handicap
-> Patience et adaptabilité 
-> Travail d'équipe
-> Rythme de travail soutenu
-> Gestion des émotions
-> Empathie
-> Gestion de problème
